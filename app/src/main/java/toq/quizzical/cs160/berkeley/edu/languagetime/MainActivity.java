package toq.quizzical.cs160.berkeley.edu.languagetime;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.qualcomm.toq.smartwatch.api.v1.deckofcards.DeckOfCardsEventListener;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.ListCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.NotificationTextCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.card.SimpleTextCard;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.DeckOfCardsManager;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCards;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteDeckOfCardsException;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteResourceStore;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.remote.RemoteToqNotification;
import com.qualcomm.toq.smartwatch.api.v1.deckofcards.resource.CardImage;

import java.io.InputStream;


public class MainActivity extends Activity {

    private DeckOfCardsManager deckOfCardsManager;
    private RemoteDeckOfCards remoteDeckOfCards;
    private RemoteResourceStore remoteResourceStore;
    private CardImage[] cardImages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        deckOfCardsManager = DeckOfCardsManager.getInstance(getApplicationContext());
        init();

        setContentView(R.layout.activity_main);

        //button to trigger notification Wizard of Oz style
        final Button notificationButton = (Button) findViewById(R.id.notificationButton);
        notificationButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                createNotification();
            }
        });

    }

    /**
     * @see android.app.Activity#onStart()
     * This is called after onCreate(Bundle) or after onRestart() if the activity has been stopped
     */
    protected void onStart(){
        super.onStart();

        if (!deckOfCardsManager.isConnected()){
            try{
                deckOfCardsManager.connect();
            }
            catch (RemoteDeckOfCardsException e){
                e.printStackTrace();
            }
        }

        install();
    }

    /**
     * Installs applet to Toq watch if app is not yet installed
     */
    private void install() {
        boolean isInstalled = true;

        try {
            isInstalled = deckOfCardsManager.isInstalled();
        }
        catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
        }

        if (!isInstalled) {
            try {
                deckOfCardsManager.installDeckOfCards(remoteDeckOfCards, remoteResourceStore);
            } catch (RemoteDeckOfCardsException e) {
                e.printStackTrace();
            }
        }
    }

    // Initialise
    private void init(){

        // Create the resource store for icons and images
        remoteResourceStore= new RemoteResourceStore();

        // Make sure in usable state
        if (remoteDeckOfCards == null){
            remoteDeckOfCards = createDeckOfCards();
        }
    }

    private RemoteDeckOfCards createDeckOfCards(){
        //create three card images for the different screens
        cardImages = new CardImage[3];
        try {
            cardImages[0] = new CardImage("card.image.1", getBitmap("takequiz.png"));
            cardImages[1] = new CardImage("card.image.2", getBitmap("hint.png"));
            cardImages[2] = new CardImage("card.image.3", getBitmap("checkanswer.png"));
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        //create the three cards, add the cardImage to each, and add to the listCard
        ListCard listCard= new ListCard();

        SimpleTextCard simpleTextCard= new SimpleTextCard("card0");
        simpleTextCard.setHeaderText("Take Quiz");
        simpleTextCard.setCardImage(remoteResourceStore, cardImages[0]);
        simpleTextCard.setReceivingEvents(true);
        listCard.add(simpleTextCard);

        simpleTextCard= new SimpleTextCard("card1");
        simpleTextCard.setHeaderText("Use Hint");
        simpleTextCard.setCardImage(remoteResourceStore, cardImages[1]);
        simpleTextCard.setReceivingEvents(true);
        listCard.add(simpleTextCard);

        simpleTextCard= new SimpleTextCard("card2");
        simpleTextCard.setHeaderText("Check Results");
        simpleTextCard.setCardImage(remoteResourceStore, cardImages[2]);
        simpleTextCard.setReceivingEvents(true);
        listCard.add(simpleTextCard);

        return new RemoteDeckOfCards(this, listCard);
    }

    //send a random notification to the Toq to tell the user they should take a quiz
    private void createNotification() {
        NotificationTextCard notificationCard;

        String[] message = new String[1];
        message[0] = "Visit the Accent app for your quiz!";

        notificationCard = new NotificationTextCard(System.currentTimeMillis(), "Quiz Time", message);

        notificationCard.setShowDivider(true);
        notificationCard.setVibeAlert(true);
        RemoteToqNotification notification = new RemoteToqNotification(this, notificationCard);

        //send the notification
        try {
            deckOfCardsManager.sendNotification(notification);
        } catch (RemoteDeckOfCardsException e) {
            e.printStackTrace();
        }
    }

    // Read an image from assets and return as a bitmap
    private Bitmap getBitmap(String fileName) throws Exception{

        try{
            InputStream is= getAssets().open(fileName);
            return BitmapFactory.decodeStream(is);
        }
        catch (Exception e){
            throw new Exception("An error occurred getting the bitmap: " + fileName, e);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
